<?php
/**
 * @file
 *
 */

function anatomy_page() {
  $build['anatomy_filter_form'] = drupal_get_form('anatomy_filter_form');

  // Load library and plugins
  drupal_add_js('http://d3js.org/d3.v3.min.js');
  drupal_add_js('http://cpettitt.github.io/project/dagre-d3/latest/dagre-d3.min.js');
  drupal_add_js('http://cpettitt.github.io/project/graphlib-dot/latest/graphlib-dot.min.js');

  // Anatomy JS an CSS
  drupal_add_js(drupal_get_path('module', 'anatomy') . '/anatomy.js', 'file');
  drupal_add_css(drupal_get_path('module', 'anatomy') . '/anatomy.css');


  // NODES
  $all_entities = entity_get_info();
  $edges = array();
  $nodes = array();
  $output = '';

  $filter = isset($_SESSION['anatomy_page_filter']['entity_types']) ? $_SESSION['anatomy_page_filter']['entity_types'] : array();

  foreach ($filter as $entity) {
    foreach ($all_entities[$entity]['bundles'] as $key => $bundle) {
      $nodes[] = array(
        'node' => $key,
        'label' => $bundle['label'],
        'group' => $all_entities[$entity]['label'],
        'shape' => isset($_SESSION['anatomy_page_filter']['settings']['export']['shape'][$entity]) ? $_SESSION['anatomy_page_filter']['settings']['export']['shape'][$entity] : 'box',
      );
      // Connection between comments and nodes
      if (isset($bundle['node bundle'])) {
       $edges[] = array(
          'from' => $key,
          'to' => $bundle['node bundle'],
          'label' => '',
        );
      }
    }
  }

  // Get edges data
  foreach (field_info_fields() as $field_name => $field) {
    if ($field['type'] === 'entityreference' || $field['type'] === 'taxonomy_term_reference') {
      foreach ($field['bundles'] as $type => $bundle) {
        if (in_array($type, $filter)) {
          $target_bundles = array();
          foreach ($bundle as $type) {
            switch ($field['type']) {
              case 'taxonomy_term_reference':
                  foreach ($bundle as $type) {
                    $target_bundles[] = $field['settings']['allowed_values'][0]['vocabulary'];
                  }
                break;
              case 'entityreference':
                  // Handle reference filtered by views
                  if ($field['settings']['handler'] === 'views') {
                    $view_name = $field['settings']['handler_settings']['view']['view_name'];
                    $display = $field['settings']['handler_settings']['view']['display_name'];
                    $view = views_get_view($view_name, $display);
                    if (!isset($view->display[$display]->display_options['filters']['type'])) $display = 'default';
                    $target_bundles = $view->display[$display]->display_options['filters']['type']['value'];
                  }
                  else {
                    $target_bundles = $field['settings']['handler_settings']['target_bundles'];
                  }
                break;
            }
            foreach ($target_bundles as $target_bundle) {
             $edges[] = array(
                'from' => $type,
                'to' => $target_bundle,
                'label' => $field_name,
              );
            }
          }
        }
      }
    }
  }

  // Create DOT data from nodes
  $output .= "//Nodes\n";
  foreach ($nodes as $node) {
    $attributes = drupal_attributes(
      array(
        'label' => $node['label'],
        'shape' => $node['shape'],
      )
    );
    $output .= dot_node($node['node'], $attributes);
  }

  // Create DOT data from edges
  $output .= "//Edges\n";
  foreach ($edges as $edge) {
    $output .= dot_connect($edge['from'], $edge['to'], $edge['label']);
  }

  $build['graph'] = array(
    '#theme' => 'anatomy',
    '#data' => $output,
  );

  return $build;
}

function anatomy_export() {
   drupal_add_http_header('Content-Type', 'text/plain; charset=utf-8');
   $graph_data = anatomy_page();
   $site_name = variable_get('site_name');

  // Set the filename.
  $filename = str_replace(' ', '_', strtolower(strip_tags($site_name . " site structure")));
  drupal_add_http_header('Content-Disposition', 'attachment; filename="' . $filename . '.dot"');


   echo "digraph graphname {\n"
   . $graph_data['graph']['#data'] .
   "}";

  exit();
}

/**
 *  Create DOT ready nodes
 *
 * @param string $node
 *   The unique id of the DOT node
 * @param string $label
 *   (optional) Label to be shown on the node
 *
 * @return string
 *   A sting with DOT ready data with information about the node
 */
function dot_node($node, $attributes = '') {
  return $node . "[$attributes]" . ";\n";
}

/**
 *  Connect two nodes.
 *
 * @param string $from
 *   The unique id of the DOT node from where the reference should point
 * @param string $to
 *   The unique id of the DOT node to where the reference should point
 * @param array $label
 *   (optional) Attributes for the reference
 *
 * @return string
 *   A sting with DOT ready data with information about the reference
 */
function dot_connect($from, $to, $label = '') {
  if (isset($_SESSION['anatomy_page_filter']['settings']['show_fields']) && $_SESSION['anatomy_page_filter']['settings']['show_fields']) {
    $label = !empty($label) ? "[label=$label]" : $label;
  }
  else {
    $label = '';
  }
  return $from . ' -> ' . $to . "$label;\n";
}


/**
 * Form constructor for the anatomy filter form.
 *
 * @see anatomy_filter_form_validate()
 * @see anatomy_filter_form_submit()
 * @see anatomy_page()
 *
 * @ingroup forms
 */
function anatomy_filter_form($form) {
  $all_entities = entity_get_info();
  $entities = array();
  $shapes = array(
      'box' => 'Box',
      'circle' => 'Circle',
      'diamond' => 'Diamond',
      'parallelogram' => 'Parallelogram',
    );
  foreach ($all_entities as $entity_key => $entity) {
    $entities[$entity_key] = $entity['label'];
  }

  $filter = isset($_SESSION['anatomy_page_filter']['entity_types']) ? $_SESSION['anatomy_page_filter']['entity_types'] : array();

  $form['filters']['entity_types'] = array(
    '#type' => 'checkboxes',
    '#options' => $entities,
    '#default_value' => $filter,
    '#title' => t('Entity types'),
  );

  $connections = array('entityreference');
  $form['filters']['connections'] = array(
    '#type' => 'checkboxes',
    '#options' => array('entityreference' => 'Entity reference'),
    '#default_value' => $connections,
    '#title' => t('Connections'),
    '#disabled' => TRUE,
  );

  $form['settings'] = array(
    '#type' => 'container',
    '#title' => t('Settings'),
    );

  $form['settings']['show_fields'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show fields'),
    '#default_value' => isset($_SESSION['anatomy_page_filter']['settings']['show_fields']) ? $_SESSION['anatomy_page_filter']['settings']['show_fields'] : 0,
  );

  // Export settings
  $form['settings']['export'] = array(
    '#type' => 'fieldset',
    '#title' => t('Export settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  foreach ($entities as $key => $entity) {
   $form['settings']['export']['shape'][$key] = array(
       '#type' => 'select',
       '#title' => $entity,
       '#options' => $shapes,
       '#default_value' => isset($_SESSION['anatomy_page_filter']['settings']['export']['shape'][$key]) ? $_SESSION['anatomy_page_filter']['settings']['export']['shape'][$key] : 'box',
   );
  }

  $form['actions'] = array(
    '#type' => 'actions',
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Filter'),
  );

  return $form;
}

/**
 * Form submission handler for anatomy_filter_form().
 *
 * @see anatomy_filter_form_validate()
 */
function anatomy_filter_form_submit($form, &$form_state) {
  $all_entities = entity_get_info();

  $values = $form_state['values'];
  $entities = $values['entity_types'];

  $_SESSION['anatomy_page_filter']['settings']['show_fields'] = $values['show_fields'];

  $_SESSION['anatomy_page_filter']['entity_types'] = array();

  foreach ($entities as $key => $entity) {
    if ($entity !== 0) {
      $_SESSION['anatomy_page_filter']['entity_types'][$key] = $entity;
      $_SESSION['anatomy_page_filter']['settings']['export']['shape'][$key] = $values[$key];
    }
  }

  foreach ($all_entities as $key => $value) {
    $_SESSION['anatomy_page_filter']['settings']['export']['shape'][$key] = $values[$key];
  }
}
